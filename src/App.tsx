// --- Dependencies
import * as React from 'react'

// --- Components
import SearchForm from './components/SearchForm'

// --- Utils
import { searchUser } from './lib/client'

// --- Styles
import 'tachyons'

/**
 * Component
 */

const App: React.FC = () => {
  return (
    <main className="sans-serif mid-gray">
      <section className="flex w-100 ph3 ph0-ns">
        <SearchForm searchFn={searchUser} />
      </section>
    </main>
  )
}

export default App
