// --- Dependencies
import Octokit from '@octokit/rest'

/**
 * Helpers
 */

// Initialize github api client
// Using default options for now, just for the sake of this demo app
const client = new Octokit()

/**
 * Types
 */

export interface ResponseError {
  message: string
  errors?: [
    {
      message: string
      resource: string
      field: string
      code: string
    }
  ]
  documentation_url: string
}

/**
 * Functions
 */

const getErrorMessage = (error: ResponseError) =>
  error.errors ? error.errors[0].message : error.message

export const searchUsers = (q: string) =>
  client.search.users({ q, per_page: 5 })

export const searchRepos = (user: string) =>
  client.search.repos({
    q: `user:${user}`,
    sort: 'stars',
    order: 'desc',
    per_page: 3
  })

export const getUser = (username: string) =>
  client.users.getByUsername({
    username
  })

export const searchUser = async (username: string) => {
  let user
  let repos
  let error

  // Get the user data
  try {
    const userResponse = await getUser(username)

    if (userResponse) {
      user = userResponse.data
    }
  } catch (e) {
    error = getErrorMessage(e)
  }

  // If we found a user get his repos
  if (user) {
    try {
      const reposResponse = await searchRepos(username)

      if (reposResponse.data) {
        repos = reposResponse.data.items
      }
    } catch (e) {
      error = getErrorMessage(e)
    }
  }

  return { error, user, repos }
}
