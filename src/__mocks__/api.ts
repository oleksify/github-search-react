export const user = {
  email: 'devilcoders@protonmail.ch',
  login: 'devilcoders',
  id: 29921,
  node_id: 'MDQ6VXNlcjI5OTIx',
  avatar_url: 'https://avatars3.githubusercontent.com/u/29921?v=4',
  gravatar_id: '',
  url: 'https://api.github.com/users/devilcoders',
  html_url: 'https://github.com/devilcoders',
  followers_url: 'https://api.github.com/users/devilcoders/followers',
  following_url:
    'https://api.github.com/users/devilcoders/following{/other_user}',
  gists_url: 'https://api.github.com/users/devilcoders/gists{/gist_id}',
  starred_url:
    'https://api.github.com/users/devilcoders/starred{/owner}{/repo}',
  subscriptions_url: 'https://api.github.com/users/devilcoders/subscriptions',
  organizations_url: 'https://api.github.com/users/devilcoders/orgs',
  repos_url: 'https://api.github.com/users/devilcoders/repos',
  events_url: 'https://api.github.com/users/devilcoders/events{/privacy}',
  received_events_url:
    'https://api.github.com/users/devilcoders/received_events',
  type: 'User',
  site_admin: false,
  name: 'Alexey Topolyanskiy',
  company: 'Spark Solutions',
  blog: 'https://alexify.me',
  location: 'Warsaw, Poland',
  hireable: false,
  bio: 'Web Designer & Developer. Photographer & Musician.',
  public_repos: 91,
  public_gists: 7,
  followers: 66,
  following: 52,
  created_at: '2008-10-20T09:35:00Z',
  updated_at: '2019-11-17T14:43:07Z'
}

export const error422 = {
  message: 'Validation Failed',
  errors: [
    {
      message:
        'The listed users and repositories cannot be searched either because the resources do not exist or you do not have permission to view them.',
      resource: 'Search',
      field: 'q',
      code: 'invalid'
    }
  ],
  documentation_url: 'https://developer.github.com/v3/search/'
}

export const error404 = {
  message: 'Not Found',
  documentation_url: 'https://developer.github.com/v3/users/#get-a-single-user'
}

export const repos = [
  {
    id: 88506736,
    node_id: 'MDEwOlJlcG9zaXRvcnk4ODUwNjczNg==',
    name: 'tachyons-for-js',
    full_name: 'devilcoders/tachyons-for-js',
    private: false,
    owner: {
      login: 'devilcoders',
      id: 29921,
      node_id: 'MDQ6VXNlcjI5OTIx',
      avatar_url: 'https://avatars3.githubusercontent.com/u/29921?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/devilcoders',
      html_url: 'https://github.com/devilcoders',
      followers_url: 'https://api.github.com/users/devilcoders/followers',
      following_url:
        'https://api.github.com/users/devilcoders/following{/other_user}',
      gists_url: 'https://api.github.com/users/devilcoders/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/devilcoders/starred{/owner}{/repo}',
      subscriptions_url:
        'https://api.github.com/users/devilcoders/subscriptions',
      organizations_url: 'https://api.github.com/users/devilcoders/orgs',
      repos_url: 'https://api.github.com/users/devilcoders/repos',
      events_url: 'https://api.github.com/users/devilcoders/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/devilcoders/received_events',
      type: 'User',
      site_admin: false
    },
    html_url: 'https://github.com/devilcoders/tachyons-for-js',
    description: 'Tachyons for CSS-in-JS',
    fork: false,
    url: 'https://api.github.com/repos/devilcoders/tachyons-for-js',
    forks_url: 'https://api.github.com/repos/devilcoders/tachyons-for-js/forks',
    keys_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/devilcoders/tachyons-for-js/teams',
    hooks_url: 'https://api.github.com/repos/devilcoders/tachyons-for-js/hooks',
    issue_events_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/issues/events{/number}',
    events_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/events',
    assignees_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/branches{/branch}',
    tags_url: 'https://api.github.com/repos/devilcoders/tachyons-for-js/tags',
    blobs_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/languages',
    stargazers_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/stargazers',
    contributors_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/contributors',
    subscribers_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/subscribers',
    subscription_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/subscription',
    commits_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/compare/{base}...{head}',
    merges_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/merges',
    archive_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/downloads',
    issues_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/labels{/name}',
    releases_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/devilcoders/tachyons-for-js/deployments',
    created_at: '2017-04-17T12:38:56Z',
    updated_at: '2019-01-04T01:45:35Z',
    pushed_at: '2019-10-29T14:15:47Z',
    git_url: 'git://github.com/devilcoders/tachyons-for-js.git',
    ssh_url: 'git@github.com:devilcoders/tachyons-for-js.git',
    clone_url: 'https://github.com/devilcoders/tachyons-for-js.git',
    svn_url: 'https://github.com/devilcoders/tachyons-for-js',
    homepage: '',
    size: 230,
    stargazers_count: 18,
    watchers_count: 18,
    language: 'JavaScript',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 1,
    mirror_url: '',
    archived: false,
    disabled: false,
    open_issues_count: 2,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 1,
    open_issues: 2,
    watchers: 18,
    default_branch: 'master',
    score: 1,
    master_branch: ''
  },
  {
    id: 1109152,
    node_id: 'MDEwOlJlcG9zaXRvcnkxMTA5MTUy',
    master_branch: '',
    name: 'BaphometDAW',
    full_name: 'devilcoders/BaphometDAW',
    private: false,
    owner: {
      login: 'devilcoders',
      id: 29921,
      node_id: 'MDQ6VXNlcjI5OTIx',
      avatar_url: 'https://avatars3.githubusercontent.com/u/29921?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/devilcoders',
      html_url: 'https://github.com/devilcoders',
      followers_url: 'https://api.github.com/users/devilcoders/followers',
      following_url:
        'https://api.github.com/users/devilcoders/following{/other_user}',
      gists_url: 'https://api.github.com/users/devilcoders/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/devilcoders/starred{/owner}{/repo}',
      subscriptions_url:
        'https://api.github.com/users/devilcoders/subscriptions',
      organizations_url: 'https://api.github.com/users/devilcoders/orgs',
      repos_url: 'https://api.github.com/users/devilcoders/repos',
      events_url: 'https://api.github.com/users/devilcoders/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/devilcoders/received_events',
      type: 'User',
      site_admin: false
    },
    html_url: 'https://github.com/devilcoders/BaphometDAW',
    description: "Inbrowser daw (Nothing works for now! Don't try this!)",
    fork: false,
    url: 'https://api.github.com/repos/devilcoders/BaphometDAW',
    forks_url: 'https://api.github.com/repos/devilcoders/BaphometDAW/forks',
    keys_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/devilcoders/BaphometDAW/teams',
    hooks_url: 'https://api.github.com/repos/devilcoders/BaphometDAW/hooks',
    issue_events_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/issues/events{/number}',
    events_url: 'https://api.github.com/repos/devilcoders/BaphometDAW/events',
    assignees_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/branches{/branch}',
    tags_url: 'https://api.github.com/repos/devilcoders/BaphometDAW/tags',
    blobs_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/statuses/{sha}',
    languages_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/languages',
    stargazers_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/stargazers',
    contributors_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/contributors',
    subscribers_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/subscribers',
    subscription_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/subscription',
    commits_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/devilcoders/BaphometDAW/merges',
    archive_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/{archive_format}{/ref}',
    downloads_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/downloads',
    issues_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/labels{/name}',
    releases_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/devilcoders/BaphometDAW/deployments',
    created_at: '2010-11-24T13:43:46Z',
    updated_at: '2015-06-03T06:19:41Z',
    pushed_at: '2011-01-06T19:36:33Z',
    git_url: 'git://github.com/devilcoders/BaphometDAW.git',
    ssh_url: 'git@github.com:devilcoders/BaphometDAW.git',
    clone_url: 'https://github.com/devilcoders/BaphometDAW.git',
    svn_url: 'https://github.com/devilcoders/BaphometDAW',
    homepage: 'http://devilcoders.github.com/BaphometDAW/',
    size: 656,
    stargazers_count: 3,
    watchers_count: 3,
    language: 'JavaScript',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: true,
    forks_count: 0,
    mirror_url: '',
    archived: false,
    disabled: false,
    open_issues_count: 9,
    license: {
      key: 'mit',
      name: 'MIT License',
      spdx_id: 'MIT',
      url: 'https://api.github.com/licenses/mit',
      node_id: 'MDc6TGljZW5zZTEz'
    },
    forks: 0,
    open_issues: 9,
    watchers: 3,
    default_branch: 'master',
    score: 1
  },
  {
    id: 4898113,
    node_id: 'MDEwOlJlcG9zaXRvcnk0ODk4MTEz',
    master_branch: '',
    name: 'try_git',
    full_name: 'devilcoders/try_git',
    private: false,
    owner: {
      login: 'devilcoders',
      id: 29921,
      node_id: 'MDQ6VXNlcjI5OTIx',
      avatar_url: 'https://avatars3.githubusercontent.com/u/29921?v=4',
      gravatar_id: '',
      url: 'https://api.github.com/users/devilcoders',
      html_url: 'https://github.com/devilcoders',
      followers_url: 'https://api.github.com/users/devilcoders/followers',
      following_url:
        'https://api.github.com/users/devilcoders/following{/other_user}',
      gists_url: 'https://api.github.com/users/devilcoders/gists{/gist_id}',
      starred_url:
        'https://api.github.com/users/devilcoders/starred{/owner}{/repo}',
      subscriptions_url:
        'https://api.github.com/users/devilcoders/subscriptions',
      organizations_url: 'https://api.github.com/users/devilcoders/orgs',
      repos_url: 'https://api.github.com/users/devilcoders/repos',
      events_url: 'https://api.github.com/users/devilcoders/events{/privacy}',
      received_events_url:
        'https://api.github.com/users/devilcoders/received_events',
      type: 'User',
      site_admin: false
    },
    html_url: 'https://github.com/devilcoders/try_git',
    description: '',
    fork: false,
    url: 'https://api.github.com/repos/devilcoders/try_git',
    forks_url: 'https://api.github.com/repos/devilcoders/try_git/forks',
    keys_url: 'https://api.github.com/repos/devilcoders/try_git/keys{/key_id}',
    collaborators_url:
      'https://api.github.com/repos/devilcoders/try_git/collaborators{/collaborator}',
    teams_url: 'https://api.github.com/repos/devilcoders/try_git/teams',
    hooks_url: 'https://api.github.com/repos/devilcoders/try_git/hooks',
    issue_events_url:
      'https://api.github.com/repos/devilcoders/try_git/issues/events{/number}',
    events_url: 'https://api.github.com/repos/devilcoders/try_git/events',
    assignees_url:
      'https://api.github.com/repos/devilcoders/try_git/assignees{/user}',
    branches_url:
      'https://api.github.com/repos/devilcoders/try_git/branches{/branch}',
    tags_url: 'https://api.github.com/repos/devilcoders/try_git/tags',
    blobs_url:
      'https://api.github.com/repos/devilcoders/try_git/git/blobs{/sha}',
    git_tags_url:
      'https://api.github.com/repos/devilcoders/try_git/git/tags{/sha}',
    git_refs_url:
      'https://api.github.com/repos/devilcoders/try_git/git/refs{/sha}',
    trees_url:
      'https://api.github.com/repos/devilcoders/try_git/git/trees{/sha}',
    statuses_url:
      'https://api.github.com/repos/devilcoders/try_git/statuses/{sha}',
    languages_url: 'https://api.github.com/repos/devilcoders/try_git/languages',
    stargazers_url:
      'https://api.github.com/repos/devilcoders/try_git/stargazers',
    contributors_url:
      'https://api.github.com/repos/devilcoders/try_git/contributors',
    subscribers_url:
      'https://api.github.com/repos/devilcoders/try_git/subscribers',
    subscription_url:
      'https://api.github.com/repos/devilcoders/try_git/subscription',
    commits_url:
      'https://api.github.com/repos/devilcoders/try_git/commits{/sha}',
    git_commits_url:
      'https://api.github.com/repos/devilcoders/try_git/git/commits{/sha}',
    comments_url:
      'https://api.github.com/repos/devilcoders/try_git/comments{/number}',
    issue_comment_url:
      'https://api.github.com/repos/devilcoders/try_git/issues/comments{/number}',
    contents_url:
      'https://api.github.com/repos/devilcoders/try_git/contents/{+path}',
    compare_url:
      'https://api.github.com/repos/devilcoders/try_git/compare/{base}...{head}',
    merges_url: 'https://api.github.com/repos/devilcoders/try_git/merges',
    archive_url:
      'https://api.github.com/repos/devilcoders/try_git/{archive_format}{/ref}',
    downloads_url: 'https://api.github.com/repos/devilcoders/try_git/downloads',
    issues_url:
      'https://api.github.com/repos/devilcoders/try_git/issues{/number}',
    pulls_url:
      'https://api.github.com/repos/devilcoders/try_git/pulls{/number}',
    milestones_url:
      'https://api.github.com/repos/devilcoders/try_git/milestones{/number}',
    notifications_url:
      'https://api.github.com/repos/devilcoders/try_git/notifications{?since,all,participating}',
    labels_url:
      'https://api.github.com/repos/devilcoders/try_git/labels{/name}',
    releases_url:
      'https://api.github.com/repos/devilcoders/try_git/releases{/id}',
    deployments_url:
      'https://api.github.com/repos/devilcoders/try_git/deployments',
    created_at: '2012-07-05T04:53:24Z',
    updated_at: '2014-02-05T11:21:50Z',
    pushed_at: '2012-07-05T05:01:49Z',
    git_url: 'git://github.com/devilcoders/try_git.git',
    ssh_url: 'git@github.com:devilcoders/try_git.git',
    clone_url: 'https://github.com/devilcoders/try_git.git',
    svn_url: 'https://github.com/devilcoders/try_git',
    homepage: '',
    size: 84,
    stargazers_count: 1,
    watchers_count: 1,
    language: '',
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    forks_count: 0,
    mirror_url: '',
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: '',
    forks: 0,
    open_issues: 0,
    watchers: 1,
    default_branch: 'master',
    score: 1
  }
]
