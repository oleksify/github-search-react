// --- Dependencies
import * as React from 'react'
import { render } from '@testing-library/react'

// --- Components
import Avatar from '../components/Avatar'

/**
 * Helpers
 */

const SIZE = 64
const SRC = `https://i.pravatar.cc/${SIZE}`
const REQ_ATTR = ['src', 'alt', 'width', 'height']
const SIZE_ATTR = ['width', 'height']

describe('Avatar', () => {
  it('should render Avatar with default size correctly', () => {
    const { getByRole } = render(
      <Avatar src="https://i.pravatar.cc/34" alt="avatar" />
    )
    const avatar = getByRole('img')

    SIZE_ATTR.forEach(attr => expect(avatar.getAttribute(attr)).toEqual('34'))
    REQ_ATTR.forEach(attr => expect(avatar.hasAttribute(attr)))
  })

  it('should render Avatar with custom size correctly', () => {
    const { getByRole } = render(<Avatar size={SIZE} src={SRC} alt="avatar" />)
    const avatar = getByRole('img')

    SIZE_ATTR.forEach(attr =>
      expect(avatar.getAttribute(attr)).toEqual(SIZE.toString())
    )
  })
})
