// --- Dependencies
import * as React from 'react'
import { render } from '@testing-library/react'

// --- Components
import App from '../App'

it('renders required ui elements', () => {
  const { getByText, getByRole } = render(<App />)
  expect(getByText('GitHub User Search')).toBeInTheDocument()
  expect(getByRole('searchbox')).toBeInTheDocument()
})
