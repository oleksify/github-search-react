// --- Dependencies
import * as React from 'react'
import { render } from '@testing-library/react'

// --- Components
import SearchField from '../components/SearchField/index'

describe('SearchField', () => {
  const onChangeFn = jest.fn()

  it('should render empty search input', () => {
    const { getByRole, queryByRole } = render(
      <SearchField inputValue="" isLoading={false} onChange={onChangeFn} />
    )
    const input = getByRole('searchbox')
    const button = queryByRole('button')

    expect(input).toHaveAttribute('value', '')
    expect(button).toBeNull()
  })

  it('should render search input with text and reset button', () => {
    const { getByRole, queryByRole } = render(
      <SearchField
        inputValue="devilcoders"
        isLoading={false}
        onChange={onChangeFn}
      />
    )
    const input = getByRole('searchbox')
    const button = queryByRole('button')

    expect(input).toHaveAttribute('value', 'devilcoders')
    expect(button).toBeInTheDocument()
  })

  it('should render loading indicator when isLoading is true', () => {
    const { queryByTestId } = render(
      <SearchField
        inputValue="devilcoders"
        isLoading={true}
        onChange={onChangeFn}
      />
    )
    const loading = queryByTestId('loading')

    expect(loading).toBeInTheDocument()
  })
})
