// --- Dependencies
import * as React from 'react'
import { render, fireEvent, waitForElement } from '@testing-library/react'
import userEvent from '@testing-library/user-event'

// --- Mocks
import { user, repos, error422, error404 } from '../__mocks__/api'

// --- Components
import SearchForm from '../components/SearchForm'

describe('SearchForm', () => {
  const searchFn = jest.fn(() =>
    Promise.resolve({ user, repos, error: undefined })
  )

  const searchFn404 = jest.fn(() =>
    Promise.resolve({
      user: undefined,
      repos: undefined,
      error: error404.message
    })
  )

  const searchFn422 = jest.fn(() =>
    Promise.resolve({
      user: undefined,
      repos: undefined,
      error: error422.errors[0].message
    })
  )

  it('should render search form', () => {
    const { getByRole, queryByTestId } = render(
      <SearchForm searchFn={searchFn} />
    )

    expect(getByRole('form')).toBeInTheDocument()
    expect(getByRole('searchbox')).toBeInTheDocument()
    expect(getByRole('searchbox')).toHaveAttribute('value', '')
    expect(queryByTestId('loading')).toBeNull()
    expect(queryByTestId('search-error')).toBeNull()
    expect(queryByTestId('user-card')).toBeNull()
  })

  it('should call onSubmit after typing in and pressing Enter', async () => {
    const { getByRole, getByTestId } = render(
      <SearchForm searchFn={searchFn} />
    )
    userEvent.type(getByRole('searchbox'), 'devilcoders')
    fireEvent.submit(getByRole('form'))

    await waitForElement(() => getByTestId('user-card'))

    expect(searchFn.mock.calls.length).toBe(1)
  })

  it('should display user card after successful request', async () => {
    const { getByRole, getByTestId } = render(
      <SearchForm searchFn={searchFn} />
    )
    userEvent.type(getByRole('searchbox'), 'devilcoders')
    fireEvent.submit(getByRole('form'))

    const userCard = await waitForElement(() => getByTestId('user-card'))

    expect(userCard).toBeInTheDocument()
    expect(userCard).toHaveTextContent('Alexey Topolyanskiy')
    expect(userCard).toHaveTextContent('@devilcoders')
    expect(userCard).toHaveTextContent(
      'Web Designer & Developer. Photographer & Musician.'
    )

    expect(userCard.querySelectorAll('li')).toHaveLength(3)
  })

  it('should close user card on clicking cancel button', async () => {
    const { getByRole, getByTestId, container } = render(
      <SearchForm searchFn={searchFn} />
    )
    userEvent.type(getByRole('searchbox'), 'devilcoders')
    fireEvent.submit(getByRole('form'))

    await waitForElement(() => getByTestId('user-card'))

    fireEvent.click(getByTestId('cancel-button'))

    expect(container.querySelector('[data-testid="user-card"]')).toBeNull()
  })

  it('should display 404 error', async () => {
    const { getByRole, getByTestId } = render(
      <SearchForm searchFn={searchFn404} />
    )

    userEvent.type(getByRole('searchbox'), 'devilcoders')
    fireEvent.submit(getByRole('form'))

    await waitForElement(() => getByTestId('search-error'))

    expect(getByTestId('search-error')).toHaveTextContent('Not Found')
  })

  it('should display 422 error', async () => {
    const { getByRole, getByTestId } = render(
      <SearchForm searchFn={searchFn422} />
    )

    userEvent.type(getByRole('searchbox'), 'devilcoders')
    fireEvent.submit(getByRole('form'))

    await waitForElement(() => getByTestId('search-error'))

    expect(getByTestId('search-error')).toHaveTextContent(
      'The listed users and repositories cannot be searched either because the resources do not exist or you do not have permission to view them.'
    )
  })
})
