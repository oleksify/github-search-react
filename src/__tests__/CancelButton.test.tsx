// --- Dependencies
import * as React from 'react'
import { render, fireEvent } from '@testing-library/react'

// --- Components
import CancelButton from '../components/CancelButton'

describe('CancelButton', () => {
  const cancelFn = jest.fn()

  it('should call cancel function on click', () => {
    const { getByRole } = render(<CancelButton cancelFn={cancelFn} />)
    const button = getByRole('button')

    fireEvent.click(button)

    expect(cancelFn.mock.calls.length).toBe(1)
  })
})
