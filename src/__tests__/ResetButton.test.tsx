// --- Dependencies
import * as React from 'react'
import { render, fireEvent } from '@testing-library/react'

// --- Components
import ResetButton from '../components/ResetButton'

describe('ResetButton', () => {
  const onClickFn = jest.fn()

  it('should call reset function on click', () => {
    const { getByRole, queryByTestId } = render(
      <ResetButton onClick={onClickFn} />
    )
    const button = getByRole('button')

    fireEvent.click(button)

    expect(onClickFn.mock.calls.length).toBe(1)
    expect(queryByTestId('loading')).toBeNull()
  })
})
