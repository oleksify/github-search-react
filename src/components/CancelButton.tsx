// --- Dependencies
import * as React from 'react'

// --- Icons
import { ReactComponent as IconCancel } from 'feather-icons/dist/icons/x.svg'

/**
 * Types
 */

interface Props {
  cancelFn: () => void
}

/**
 * Component
 */

const CancelButton: React.FC<Props> = ({ cancelFn }) => (
  <button
    data-testid="cancel-button"
    aria-label="Close user info"
    onClick={cancelFn}
    type="button"
    className="pointer bn pa0 bg-transparent gray absolute right-1 top-1"
  >
    <IconCancel />
  </button>
)

export default CancelButton
