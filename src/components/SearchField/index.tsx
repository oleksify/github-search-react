// --- Dependencies
import * as React from 'react'

// --- Components
import ResetButton from '../ResetButton'
import Loading from '../Loading'

// --- Icons
import { ReactComponent as IconSearch } from 'feather-icons/dist/icons/search.svg'

// --- Styles
import './index.css'

/**
 * Types
 */

interface Props {
  isLoading: boolean
  inputValue: string
  onChange: (value: string) => void
}

/**
 * Component
 */

const SearchField: React.FC<Props> = ({
  isLoading,
  inputValue = '',
  onChange
}) => {
  return (
    <>
      <IconSearch className="gray absolute left-1 h-100" />
      <input
        value={inputValue}
        placeholder="Type user name and press Enter"
        type="search"
        autoComplete="off"
        autoFocus
        className="input-reset br2 ba b--moon-gray pv3 f4 w-100 dark-gray"
        aria-label="Search for github user"
        onChange={({ currentTarget }) => onChange(currentTarget.value)}
      />

      {/* Reset Button or Loading indicator is only rendered if search input is
      not an empty string */}
      {inputValue !== '' && (
        <>
          {isLoading ? (
            <Loading />
          ) : (
            <ResetButton onClick={() => onChange('')} />
          )}
        </>
      )}
    </>
  )
}

export default SearchField
