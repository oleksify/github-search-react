// --- Dependencies
import * as React from 'react'

/**
 * Types
 */

interface Props {
  src: string
  alt: string
  size?: number
}

/**
 * Component
 */

const Avatar: React.FC<Props> = ({ src, alt, size = 34 }) => (
  <img className="dib br-100" width={size} height={size} src={src} alt={alt} />
)

export default React.memo(Avatar)
