// --- Dependencies
import * as React from 'react'

/**
 * Types
 */

interface Props {
  children: React.ReactNode
}

/**
 * Component
 */

const Error: React.FC<Props> = ({ children }) => (
  <p className="bg-red white tc f5 pa2 lh-copy" data-testid="search-error">
    {children}
  </p>
)

export default React.memo(Error)
