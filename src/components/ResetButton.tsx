// --- Dependencies
import * as React from 'react'

// --- Icons
import { ReactComponent as IconCancel } from 'feather-icons/dist/icons/x-circle.svg'

/**
 * Types
 */

interface Props {
  onClick: () => void
}

/**
 * Component
 */

const ResetButton: React.FC<Props> = ({ onClick }) => (
  <button
    type="reset"
    aria-label="Reset search results"
    className="pointer absolute bg-transparent pa0 bn right-1 h-100"
    onClick={onClick}
  >
    <IconCancel className="gray h-100" />
  </button>
)

export default ResetButton
