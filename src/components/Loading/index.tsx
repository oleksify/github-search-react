// --- Dependencies
import * as React from 'react'

// --- Styles
import styles from './index.module.css'

/**
 * Component
 */

const Loading = () => (
  <div data-testid="loading" className={styles.loading}></div>
)

export default Loading
