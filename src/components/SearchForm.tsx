// --- Dependencies
import * as React from 'react'

// --- Icons
import { ReactComponent as IconGithub } from 'feather-icons/dist/icons/github.svg'

// --- Types
import {
  UsersGetByUsernameResponse,
  SearchReposResponseItemsItem
} from '@octokit/rest'

// --- Components
import SearchField from './SearchField'
import UserCard from './UserCard'
import Error from './Error'

/**
 * Types
 */

interface Props {
  searchFn: (
    username: string
  ) => Promise<{
    error: string | undefined
    user: UsersGetByUsernameResponse | undefined
    repos: SearchReposResponseItemsItem[] | undefined
  }>
}

/**
 * Component
 */

const SearchForm: React.FC<Props> = ({ searchFn }) => {
  // State Hook to hold request error
  const [error, setError] = React.useState<string | null>(null)

  // State Hook to track and handle input field state
  const [inputValue, setInputValue] = React.useState<string>('')

  // State Hook to hold xhr loading state
  const [isLoading, setLoading] = React.useState(false)

  // State Hook to hold github user search response
  const [
    userData,
    setUserData
  ] = React.useState<UsersGetByUsernameResponse | null>(null)

  // State hook to hold user repos response
  const [repos, setRepos] = React.useState<SearchReposResponseItemsItem[]>([])

  // Set user input
  const onChange = (value: string) => setInputValue(value)

  // Close user card
  const handleCancel = () => {
    setUserData(null)
    setError(null)
  }

  // Form submit function
  const onSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()

    // Do nothing if input is empty
    if (inputValue === '') return

    // Set loading state
    setLoading(true)

    // Reset previous result first
    userData && setUserData(null)
    setError(null)
    setRepos([])

    // Request user data
    const searchResults = await searchFn(inputValue)

    // Stop loading indicator
    setLoading(false)

    // Update local state
    searchResults.error && setError(searchResults.error)
    searchResults.user && setUserData(searchResults.user)
    searchResults.repos && setRepos(searchResults.repos)
  }

  return (
    <div className="mw7 w-100 center relative mt5">
      <div className="flex items-center mb2">
        <IconGithub className="dark-gray" />
        <h1 className="fw6 mv0 ml2 f3 dark-gray">GitHub User Search</h1>
      </div>
      <form
        id="github-search"
        className="w-100 flex relative"
        onSubmit={onSubmit}
      >
        <SearchField
          isLoading={isLoading}
          inputValue={inputValue}
          onChange={onChange}
        />
      </form>

      {/* User Not Found Error */}
      {error && (
        <div className="mv3 br2 overflow-hidden">
          <Error>{error}</Error>
        </div>
      )}

      {/* Render user data card when everything if finished loading */}
      {!isLoading && userData && (
        <UserCard user={userData} repos={repos} handleClose={handleCancel} />
      )}
    </div>
  )
}

export default SearchForm
