// --- Dependencies
import * as React from 'react'

// --- Components
import Avatar from './Avatar'
import CancelButton from './CancelButton'

// --- Icons
import { ReactComponent as IconLink } from 'feather-icons/dist/icons/external-link.svg'
import { ReactComponent as IconStar } from 'feather-icons/dist/icons/star.svg'

// --- Types
import {
  SearchReposResponseItemsItem,
  UsersGetByUsernameResponse
} from '@octokit/rest'

/**
 * Types
 */

interface Props {
  user: UsersGetByUsernameResponse
  repos: SearchReposResponseItemsItem[]
  handleClose: () => void
}

/**
 * Component
 */

const UserCard: React.FC<Props> = ({ user, repos, handleClose }) => (
  <output htmlFor="github-search">
    <div className="br2 ba b--moon-gray mt3 relative" data-testid="user-card">
      <CancelButton cancelFn={handleClose} />

      <div className="flex items-center pa3 bb b--light-gray">
        <Avatar src={user.avatar_url} alt={user.name} size={84} />

        <div className="ml3">
          <hgroup className="pr4 pr0-ns">
            <h2 className="f3 fw5 mv0 near-black">{user.name || user.login}</h2>
            <h3 className="mb0 mt1 f5 fw4 mid-gray">@{user.login}</h3>
          </hgroup>
          {user.bio && <p className="lh-copy mb0">{user.bio}</p>}
        </div>
      </div>

      {repos.length > 0 && (
        <>
          <h3 className="mv0 ttu f6 bb pv2 ph3 fw5 b--light-gray bg-dark-gray white">
            User top repos:
          </h3>
          <ol className="pa3 list mb0 lh-copy mt0">
            {repos.map(repo => (
              <li key={repo.id} className="mb2">
                <a
                  rel="noopener noreferrer"
                  href={repo.html_url}
                  target="_blank"
                  className="link f6 fw6 mid-gray flex items-center w-100"
                >
                  <div
                    className="flex items-center mr2"
                    title={`Repo Name: ${repo.name}`}
                  >
                    <IconLink className="flex mr2 w1 h1" />
                    {repo.name}
                  </div>
                  /
                  <div
                    className="ml2 flex items-center"
                    title={`Repo Stars: ${repo.stargazers_count}`}
                  >
                    <IconStar className="flex w1 h1 mr1" />{' '}
                    {repo.stargazers_count}
                  </div>
                </a>
              </li>
            ))}
          </ol>
        </>
      )}
    </div>
  </output>
)

export default React.memo(UserCard)
